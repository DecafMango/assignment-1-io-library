%define READ_SYSCALL 0
%define PRINT_SYSCALL 1
%define EXIT_SYSCALL 60

%define STDIN 0
%define STDOUT 1

%define ZERO_TERMINATOR 0
%define TABULATION_CHAR 0x9
%define NEWLINE_CHAR 0xA
%define SPACE_CHAR 0x20

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rdi + rax], ZERO_TERMINATOR
        je .exit
        inc rax
        jmp .loop
    .exit:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi

    mov rdx, rax
    mov rax, PRINT_SYSCALL
    mov rsi, rdi
    mov rdi, STDOUT
    syscall

    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ;Прочитаем символ со стека

    mov rax, PRINT_SYSCALL
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    js .negative
    .positve: ;Ввел эту метку для логического разделения
        jmp print_uint
    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10
    mov rcx, rsp  
    sub rsp, 32

    dec rcx
    mov byte [rcx], ZERO_TERMINATOR
    
    .loop:
        xor rdx, rdx
        div r10
        add rdx, '0' ;Перевод числа в символ ASCII
        dec rcx
	mov byte [rcx], dl
 	cmp rax, 0
        je .print
        jmp .loop
    .print:
        mov rdi, rcx
        call print_string
  	add rsp, 32	
	ret

;Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx

    .loop:
        mov r8b, byte [rdi + rcx]
        cmp r8b, byte [rsi + rcx]
        jne .false
    .chieck_zero_term:
        cmp r8b, ZERO_TERMINATOR
        je .true
        inc rcx
        jmp .loop
    .true:
        mov rax, 1
        ret
    .false:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0 ;Подготавливаем место в стеке для чтения символа

    mov rax, READ_SYSCALL
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall

    cmp rax, 0
    je .EOF
    cmp rax, -1
    je .error
    jmp .success

    .EOF:
    	pop rax
	xor rax, rax
	mov rdx, 1 ;Означает, что операция была выполнена успешно
	ret
    .error:
	pop rax
    	mov rdx, 0 ;Означает, что произошла ошибка при выполнении операции
	ret
    .success:
    	pop rax
    	mov rdx, 1 ;Означает, что операция была выполнена успешно
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
	mov r13, rsi
    xor r14, r14 ;Очистим для дальнейшего подсчета введных символов

    .skip_whitespace:
        call read_char
        cmp rax, TABULATION_CHAR
        je .skip_whitespace
        cmp rax, NEWLINE_CHAR
        je .skip_whitespace
        cmp rax, SPACE_CHAR
        je .skip_whitespace
    .read:
        cmp r14, r13
        je .fail
        cmp al, 0
        je .success
        cmp al, TABULATION_CHAR
        je .success
        cmp al, NEWLINE_CHAR
        je .success
        cmp al, SPACE_CHAR
        je .success

        mov byte [r12 + r14], al
        inc r14

        call read_char

        jmp .read
    .success:
        mov byte [r12 + r14], ZERO_TERMINATOR
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret
    .fail:
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ;Используем для накопления результата
    mov r10, 10 ;Используем в качестве множителя
    xor r8, r8 ;Используем для подсчета длины
    xor r9, r9 ;Используем в качестве буффера для символов

    .loop:
        mov r9b, byte [rdi + r8]
        cmp r9b, ZERO_TERMINATOR
        je .exit
        cmp r9b, '0'
        jl .exit
        cmp r9b, '9'
        jg .exit

        inc r8
        sub r9b, '0'
        mul r10
        add rax, r9
        jmp .loop
    .exit:
        mov rdx, r8
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r9, r9 ;Используем в качестве буффера для символов

    mov r9b, byte [rdi]
    cmp r9b, '-'
    je .negative

        push rdi
        call parse_uint
        pop rdi
        test r9b, '+'
        jne .exit
        inc rdx
        jmp .exit
    .negative:
        inc rdi
        push rdi
        call parse_uint
        pop rdi
        neg rax
        inc rdx
    .exit:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length ;В rax хранится длина переданной строки
    pop rdx
    pop rsi
    pop rdi

    cmp rax, rdx
    jg .fail

    xor rax, rax ;Теперь будем использовать rax в качестве счетчика символов
    xor r8, r8 ;Используем в качестве буффера символов
    .loop:
        mov r8b, byte [rdi + rax]
        mov byte [rsi + rax], r8b
        cmp r8b, ZERO_TERMINATOR
        je .success
        inc rax
        jmp .loop
    .success:
        ret
    .fail:
        xor rax, rax
        ret
